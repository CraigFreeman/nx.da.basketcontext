﻿using System;
using System.Collections.Generic;

namespace McCann.NX.DA.BasketContext.Model
{
    public class Basket : MongoBase
    {
        public Basket() {
            List<BasketItem> basketItems = new List<BasketItem>();
            List<Voucher> vouchers = new List<Voucher>();
        }
        
        public string accountId { get; set; }
        public string deviceId { get; set; }
        public decimal totalItems { get; set; }
        public decimal totalVouchers { get; set; }
        public decimal totalBasket { get; set; }
        
        public List<BasketItem> basketItems { get; set; }
        public List<Voucher> vouchers { get; set; }
    }

    public class BasketItem : MongoBase
    {
        public BasketItem()
        {
            List<BasketItemProperty> properties = new List<BasketItemProperty>();
        }

        public int quantity { get; set; }
        public decimal price { get; set; }
        public decimal itemPrice { get; set; }
        public List<BasketItemProperty> properties { get; set; }

    }

    public class BasketItemProperty
    {
        public string name { get; set; }
        public string value { get; set; }
    }

    public class Voucher
    {
        public Voucher() {
            VoucherValue voucherValue = new VoucherValue();
        }

        public string code { get; set; }
        public string voucherType { get; set; }
        public DateTime? startDate { get; set; }
        public DateTime? endDate { get; set; }
        public List<string> products { get; set; }
        public VoucherValue voucherValue { get; set; }
    }

    public class VoucherValue
    {
        public string valueType { get; set; }
        public decimal amount { get; set; }
    }

}
