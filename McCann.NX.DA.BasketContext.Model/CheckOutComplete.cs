﻿using System;
using System.Collections.Generic;
using System.Text;

namespace McCann.NX.DA.BasketContext.Model
{
    public class CheckOutComplete
    {
        public bool success { get; set; }
        public string transactionId { get; set; }
    }
}
