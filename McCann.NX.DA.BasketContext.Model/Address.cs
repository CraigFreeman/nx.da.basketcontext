﻿using System;
using System.Collections.Generic;
using System.Text;

namespace McCann.NX.DA.BasketContext.Model
{
    public class Address
    {
        public string department { get; set; }
        public string company { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }
        public string address3 { get; set; }
        public string town { get; set; }
        public string county { get; set; }
        public string postCode { get; set; }
        public string country { get; set; }
        public string localAuthority { get; set; }
    }
}
