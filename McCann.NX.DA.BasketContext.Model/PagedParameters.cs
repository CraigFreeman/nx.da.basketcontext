﻿using System;
using System.Collections.Generic;
using System.Text;

namespace McCann.NX.DA.BasketContext.Model
{
    public class PagedParameters
    {

        private int m_take;
        public int skip { get; set; }

        public int take
        {
            get
            {
                if (m_take == 0)
                    return 10;
                else
                    return m_take;
            }
            set
            {
                if (value == 0)
                    m_take = 10;
                else
                    m_take = value;
            }
        }
    }
}
