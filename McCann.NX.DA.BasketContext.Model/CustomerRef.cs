﻿using System;
using System.Collections.Generic;
using System.Text;

namespace McCann.NX.DA.BasketContext.Model
{
    public class CustomerRef : MongoBase
    {
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string title { get; set; }
        public string emailAddress { get; set; }
    }
}
