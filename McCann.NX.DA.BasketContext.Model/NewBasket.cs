﻿using System;
using System.Collections.Generic;
using System.Text;

namespace McCann.NX.DA.BasketContext.Model
{
    public class NewBasket
    {
        public string accountId { get; set; }
        public string deviceId { get; set; }
    }
}
