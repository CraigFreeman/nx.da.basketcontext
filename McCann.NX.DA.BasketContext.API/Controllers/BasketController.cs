﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.SwaggerGen;
using McCann.NX.DA.BasketContext.Model;


namespace McCann.NX.DA.BasketContext.API.Controllers
{
    /// <summary>
    /// Accounts controller
    /// </summary>
    [ApiVersion("1")]
    [Produces("application/json")]
    public class BasketController : Controller
    {
        McCann.NX.DA.BasketContext.Service.Interface.IBasketService _basketService;

        public BasketController(McCann.NX.DA.BasketContext.Service.Interface.IBasketService basketService)
        {
            _basketService = basketService;
        }

        /// <summary>
        /// Gets a Basket By Account or Device
        /// </summary>
        /// <param name="accountId">The account id of which the basket relates</param>
        /// <param name="deviceId">The account id of which the basket relates</param>
        /// <returns></returns>
        /// <response code="200">The basket object</response>
        /// <response code="404">If the basket could not be found</response>
        [SwaggerOperation("Gets a basket")]
        [ProducesResponseType(typeof(Basket), 200)]
        [Route("/api/{version:apiVersion}/{vendor}/baskets/")]
        [HttpGet]
        public async Task<Object> GetBasketAsync([FromQuery]string accountId, [FromQuery]string deviceId)
        {
            var x = await _basketService.GetBasketAsync(accountId, deviceId);
            if (x != null)
            {
                return Ok(x);
            }
            else
            {
                return NotFound();
            }
        }

        /// <summary>
        /// Gets a Basket By Unique Id
        /// </summary>
        /// <param name="id">The basket id of which the basket relates</param>
        /// <returns></returns>
        /// <response code="200">The basket object</response>
        /// <response code="404">If the basket could not be found</response>
        [SwaggerOperation("Gets a basket")]
        [ProducesResponseType(typeof(Basket), 200)]
        [Route("/api/{version:apiVersion}/{vendor}/baskets/{id}")]
        [HttpGet]
        public async Task<Object> GetBasketAsync(string id)
        {
            var x = await _basketService.GetBasketAsyncByID(id);
            if (x != null)
            {
                return Ok(x);
            }
            else
            {
                return NotFound();
            }
        }

        /// <summary>
        /// Create a Basket
        /// </summary>
        /// <param name="body">The New Basket Object</param>
        /// <returns></returns>
        /// <response code="200">The basket object</response>
        /// <response code="404">If the basket could not be found</response>
        [SwaggerOperation("Creates a basket")]
        [ProducesResponseType(typeof(Basket), 200)]
        [Route("/api/{version:apiVersion}/{vendor}/baskets/")]
        [HttpPost]
        public async Task<Object> CreateBasket([FromBody]NewBasket body)
        {
            var x = await _basketService.CreateBasketAsync(body.accountId, body.deviceId);
            if (x != null)
            {
                return Ok(x);
            }
            else
            {
                return NotFound();
            }
        }

        /// <summary>
        /// Gets all items in a basket
        /// </summary>
        /// <param name="id">Returns all items within a specified basket</param>
        /// <returns></returns>
        /// <response code="200">The basket object</response>
        /// <response code="404">If the basket could not be found</response>
        [SwaggerOperation("Gets all items in a basket")]
        [ProducesResponseType(typeof(Basket), 200)]
        [Route("/api/{version:apiVersion}/{vendor}/baskets/{id}/items")]
        [HttpGet]
        public async Task<Object> GetBasketItemsAsync(string id)
        {
            var x = await _basketService.GetBasketAsyncByID(id);
            if (x != null)
            {
                if (x.basketItems == null)
                    return Ok(new List<BasketItem>());
                else
                    return Ok(x.basketItems);
            }
            else
            {
                return NotFound();
            }
        }

        /// <summary>
        /// Inserts a new basket item.
        /// </summary>
        /// <param name="id">The ID of the basket to be Updated</param>
        /// <param name="body">New basket item to be added</param>
        /// <returns></returns>
        /// <response code="200">The basket object</response>
        /// <response code="404">If the basket could not be found</response>
        [SwaggerOperation("Add an item to a Basket")]
        [ProducesResponseType(typeof(Basket), 200)]
        [Route("/api/{version:apiVersion}/{vendor}/baskets/{id}/items")]
        [HttpPost]
        public async Task<Object> AddBasketItemToBasket(string id, [FromBody]BasketItem body)
        {
            var x = await _basketService.AddItemToBasketAsync(id, body);
            if (x != null)
            {
                return Ok(x);
            }
            else
            {
                return NotFound();
            }
        }

        /// <summary>
        /// Update an Item in a Basket
        /// </summary>
        /// <param name="itemId">The ID of the basket item to be Updated</param>
        /// <param name="basketId">The ID of the basket to be Updated</param>
        /// <param name="body">basket item to be added</param>
        /// <returns></returns>
        /// <response code="200">The basket object</response>
        /// <response code="404">If the basket could not be found</response>
        [SwaggerOperation("Update an Item in a Basket")]
        [ProducesResponseType(typeof(Basket), 200)]
        [Route("/api/{version:apiVersion}/{vendor}/baskets/{basketId}/items/{itemId}")]
        [HttpPut]
        public async Task<Object> UpdateBasketItemToBasket(string itemId, string basketId, [FromBody]BasketItem body)
        {
            var x = await _basketService.UpdateBasketItemAsync(itemId, basketId, body);
            if (x != null)
            {
                return Ok(x);
            }
            else
            {
                return NotFound();
            }
        }

        /// <summary>
        /// Remove an item from the Basket
        /// </summary>
        /// <param name="itemId">The ID of the basket item to be Deleted</param>
        /// <param name="basketId">The ID of the basket to be Updated</param>
        /// <returns></returns>
        /// <response code="200">The basket object</response>
        /// <response code="404">If the basket could not be found</response>
        [SwaggerOperation("Delete an Item in a Basket")]
        [ProducesResponseType(typeof(Basket), 200)]
        [Route("/api/{version:apiVersion}/{vendor}/baskets/{basketId}/items/{itemId}")]
        [HttpDelete]
        public async Task<Object> DeleteBasketItemToBasket(string itemId, string basketId)
        {
            var x = await _basketService.DeleteBasketItemAsync(itemId, basketId);
            if (x != null)
            {
                return Ok(x);
            }
            else
            {
                return NotFound();
            }
        }

        /// <summary>
        /// Calcualte Basket.
        /// </summary>
        /// <param name="basketId">The ID of the basket to be Calculated</param>
        /// <returns></returns>
        /// <response code="200">The basket object</response>
        /// <response code="404">If the basket could not be found</response>
        [SwaggerOperation("Calculate Basket Totals")]
        [ProducesResponseType(typeof(Basket), 200)]
        [Route("/api/{version:apiVersion}/{vendor}/baskets/{basketId}/calculate")]
        [HttpPost]
        public async Task<Object> CalculateBasket (string basketId)
        {
            var x = await _basketService.CalculateBasket(basketId);
            if (x != null)
            {
                return Ok(x);
            }
            else
            {
                return NotFound();
            }
        }

        /// <summary>
        /// Add Voucher to Basket
        /// </summary>
        /// <param name="basketId">The ID of the basket for the new Voucher</param>
        /// <param name="body">voucher item to be added</param>
        /// <returns></returns>
        /// <response code="200">The basket object</response>
        /// <response code="404">If the basket could not be found</response>
        [SwaggerOperation("Add Voucher")]
        [ProducesResponseType(typeof(Basket), 200)]
        [Route("/api/{version:apiVersion}/{vendor}/baskets/{basketId}/vouchers")]
        [HttpPost]
        public async Task<Object> DeleteBasketItemToBasket(string basketId, [FromBody]Voucher body)
        {
            var x = await _basketService.AddVoucherToBasket(basketId, body);
            if (x != null)
            {
                return Ok(x);
            }
            else
            {
                return NotFound();
            }
        }

        /// <summary>
        /// Get Vouchers for Basket
        /// </summary>
        /// <param name="basketId">The ID of the basket</param>
        /// <returns></returns>
        /// <response code="200">The basket object</response>
        /// <response code="404">If the basket could not be found</response>
        [SwaggerOperation("Get Vouchers")]
        [ProducesResponseType(typeof(Voucher), 200)]
        [Route("/api/{version:apiVersion}/{vendor}/baskets/{basketId}/vouchers")]
        [HttpGet]
        public async Task<Object> GetVouchersForBasket(string basketId)
        {
            var x = await _basketService.GetVouchersForBasket(basketId);
            if (x != null)
            {
                return Ok(x);
            }
            else
            {
                return NotFound();
            }
        }

        /// <summary>
        /// Update Voucher for Basket
        /// </summary>
        /// <param name="basketId">The ID of the basket</param>
        /// <param name="body">voucher item to be updated</param>
        /// <returns></returns>
        /// <response code="200">The basket object</response>
        /// <response code="404">If the basket could not be found</response>
        [SwaggerOperation("Update Voucher")]
        [ProducesResponseType(typeof(Voucher), 200)]
        [Route("/api/{version:apiVersion}/{vendor}/baskets/{basketId}/vouchers")]
        [HttpPut]
        public async Task<Object> UpdateVoucherInBasket(string basketId, [FromBody]Voucher body)
        {
            var x = await _basketService.UpdateVoucher(basketId, body);
            if (x != null)
            {
                return Ok(x);
            }
            else
            {
                return NotFound();
            }
        }
    }
}
