﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using McCann.NX.DA.BasketContext.Model;

namespace McCann.NX.DA.BasketContext.Service.Interface
{
    public interface IBasketService
    {
        Task<Basket> GetBasketAsync(string id, string deviceId);
        Task<Basket> CreateBasketAsync(string id, string deviceId);
        Task<Basket> UpdateBasketAsync(string id, Basket basket);
        Task<Basket> GetBasketAsyncByID(string id);
        Task<Basket> AddItemToBasketAsync(string id, BasketItem item);
        Task<Basket> UpdateBasketItemAsync(string itemId, string basketId, BasketItem item);
        Task<Basket> DeleteBasketItemAsync(string itemId, string basketId);
        Task<Basket> CalculateBasket(string basketId);
        Task<Basket> AddVoucherToBasket(string basketId, Voucher voucher);
        Task<List<Voucher>> GetVouchersForBasket(string basketId);
        Task<Voucher> UpdateVoucher(string basketId, Voucher voucher);
    }
}
