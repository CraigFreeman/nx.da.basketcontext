﻿using System;
using System.Collections.Generic;
using System.Text;
using MongoDB;
using MongoDB.Driver;
using MongoDB.Bson;
using System.Threading.Tasks;
using System.Linq;
using McCann.NX.DA.BasketContext.Model;


namespace McCann.NX.DA.BasketContext.Service.Repository
{
    public class MongoDB
    {
        private const string endpointUrl = "mongodb://nxbusmongodb:wmctgvXkgbAfbnozvHAPDA7JwMNNENw967dEEOpvXDB52HZImPXcOkpoOMwqogOHETsuA9PX9JcR87AjZ0mR4w==@nxbusmongodb.documents.azure.com:10255/?ssl=true&replicaSet=globaldb";
        //private const string endpointUrl = "mongodb://localhost:C2y6yDjf5/R+ob0N8A7Cgv30VRDJIWEHLM+4QDU5DE2nQ9nDuVTqobD4b8mGGyPMbIZnqyMsEcaGQy67XIw/Jw==@localhost:10255/admin?ssl=true";
        private const string primaryKey = "wmctgvXkgbAfbnozvHAPDA7JwMNNENw967dEEOpvXDB52HZImPXcOkpoOMwqogOHETsuA9PX9JcR87AjZ0mR4w==";
        private string _DatabaseName = "BasketDB";
        private string _DocumentCollectionName = "BasketCollection";

        private MongoClientSettings settings = null;
        private MongoClient client = null;
        private IMongoDatabase _database;

        public MongoDB()
        {
            MongoUrl mongoUrl = new MongoUrl(endpointUrl);
            settings = MongoClientSettings.FromUrl(mongoUrl);
            settings.SslSettings = new SslSettings { EnabledSslProtocols = System.Security.Authentication.SslProtocols.Tls12 };


            var client = new MongoClient(settings);
            _database = client.GetDatabase(_DatabaseName);
        }

        private async Task CreateDatabase()
        {

        }

        public async Task SaveObject<T>(T item, string collectionName)
        {
            var collection = _database.GetCollection<T>(collectionName);

            try { await collection.InsertOneAsync(item); }
            catch (Exception ex) { throw new Exception(ex.Message); }


        }

        public async Task SaveObjects<T>(T[] items, string collectionName)
        {
            var collection = _database.GetCollection<T>(collectionName);
            await collection.InsertManyAsync(items);
        }


        public async Task UpdateObject<T>(T item, string collectionName) where T : MongoBase
        {
            var collection = _database.GetCollection<T>(collectionName);
            var filter = Builders<T>.Filter.Eq("id", item.id);

            await collection.ReplaceOneAsync<T>(x => x.id == item.id, item);
        }

        public async Task<T> GetObject<T>(string id, string collectionName) where T : MongoBase
        {

            try
            {
                var collection = _database.GetCollection<T>(collectionName);
                var filter = Builders<T>.Filter.Eq("id", id);

                return collection.Find(filter).ToListAsync<T>().Result.FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }

        public async Task<T> GetObjectByAccountId<T>(string param, string collectionName) where T : MongoBase
        {
            var collection = _database.GetCollection<T>(collectionName);
            var filter = Builders<T>.Filter.Eq("accountId", param);

            return collection.Find(filter).ToListAsync<T>().Result.FirstOrDefault();
        }

        public async Task<T> GetObjectByDeviceId<T>(string param, string collectionName) where T : MongoBase
        {
            var collection = _database.GetCollection<T>(collectionName);
            var filter = Builders<T>.Filter.Eq("deviceId", param);

            return collection.Find(filter).ToListAsync<T>().Result.FirstOrDefault();
        }


        public async Task<List<T>> GetObjects<T>(string collectionName) where T : MongoBase
        {
            var collection = _database.GetCollection<T>(collectionName);

            //return collection.AsQueryable<T>().Skip(2).Take(2).ToArray<T>(); // test for mongodb skip/take
            return collection.AsQueryable<T>().ToList<T>();
        }

        public async Task<List<T>> GetObjects<T>(string collectionName, PagedParameters param) where T : MongoBase
        {
            var collection = _database.GetCollection<T>(collectionName);
            return collection.AsQueryable<T>().Skip(param.skip).Take(param.take).ToList<T>(); // test for mongodb skip/take
        }

        public async Task<bool> DeleteObject<T>(string id, string collectionName) where T : MongoBase
        {
            var collection = _database.GetCollection<T>(collectionName);

            var filter = Builders<T>.Filter.Eq("id", id);
            DeleteResult _result = await collection.DeleteOneAsync<T>(x => x.id == id);

            return _result.IsAcknowledged;
        }

    }
}
