﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using McCann.NX.DA.BasketContext.Model;

namespace McCann.NX.DA.BasketContext.Service.Implementation
{
    public class BasketService : Interface.IBasketService
    {
        public Repository.MongoDB _repository;

        public BasketService()
        {
            _repository = new Repository.MongoDB();
        }

        public async Task<Basket> CreateBasketAsync(string id, string deviceId)
        {
            string uniqueId = Guid.NewGuid().ToString();
            Basket basket = new Basket() { id = uniqueId, accountId = id, deviceId = deviceId };
            await _repository.SaveObject(basket, "Basket");
            return await GetBasketAsyncByID(uniqueId);
        }

        public async Task<Basket> GetBasketAsyncByID(string id)
        {
            return await _repository.GetObject<Basket>(id, "Basket");
        }

        public async Task<Basket> GetBasketAsync(string id, string deviceId)
        {
            if (id != null)
                return await _repository.GetObjectByAccountId<Basket>(id, "Basket");
            else
                return await _repository.GetObjectByDeviceId<Basket>(deviceId, "Basket");
        }

        public async Task<Basket> UpdateBasketAsync(string id, Basket basket)
        {
            await _repository.UpdateObject<Basket>(basket, "Basket");
            return await GetBasketAsyncByID(id);
        }

        public async Task<Basket> AddItemToBasketAsync(string id, BasketItem item)
        {
            Basket basket = await _repository.GetObject<Basket>(id, "Basket");
            if (basket != null)
            {
                //Check to see if the item is already in the basket...
                if (basket.basketItems != null)
                {
                    foreach (BasketItem i in basket.basketItems)
                    {
                        if (i.id == item.id)
                        {
                            //Already added, update quantity...
                            i.quantity = item.quantity;
                            i.price = Convert.ToInt16(item.itemPrice * item.quantity);
                            await _repository.UpdateObject<Basket>(basket, "Basket");
                            return await GetBasketAsyncByID(basket.id);
                        }
                    }
                }
                else
                {
                    basket.basketItems = new List<BasketItem>();
                }

                basket.basketItems.Add(item);
                await _repository.UpdateObject<Basket>(basket, "Basket");
                return await GetBasketAsyncByID(basket.id);

            }
            else
                return null;
        }

        public async Task<Basket> UpdateBasketItemAsync(string itemId, string basketId, BasketItem item)
        {
            List<BasketItem> updateList = new List<BasketItem>();
            Basket basket = await _repository.GetObject<Basket>(basketId, "Basket");
            if (basket != null)
            {
                //Check to see if the item is already in the basket...
                if (basket.basketItems != null)
                {
                    foreach (BasketItem i in basket.basketItems)
                    {

                        if (i.id == itemId)
                        {
                            updateList.Add(item);
                        }
                        else
                        {
                            updateList.Add(i);
                        }
                    }
                }
                else
                {
                    updateList.Add(item);
                }
                basket.basketItems = updateList;
                await _repository.UpdateObject<Basket>(basket, "Basket");
                return await GetBasketAsyncByID(basket.id);
            }
            else
                return null;
        }

        public async Task<Basket> DeleteBasketItemAsync(string itemId, string basketId)
        {
            List<BasketItem> updateList = new List<BasketItem>();
            Basket basket = await _repository.GetObject<Basket>(basketId, "Basket");
            if (basket != null)
            {
                //Check to see if the item is already in the basket...
                if (basket.basketItems != null)
                {
                    foreach (BasketItem i in basket.basketItems)
                    {

                        if (i.id != itemId)
                        {
                            updateList.Add(i);
                        }
                    }
                    basket.basketItems = updateList;
                    await _repository.UpdateObject<Basket>(basket, "Basket");
                    return await GetBasketAsyncByID(basket.id);
                }
                else
                    return await GetBasketAsyncByID(basket.id);
            }
            else
                return null;
        }

        public async Task<Basket> CalculateBasket(string basketId)
        {
            Basket basket = await GetBasketAsyncByID(basketId);
            if (basket == null)
                return null;

            decimal basketTotal = 0.0m;
            decimal voucherTotal = 0.0m;
            decimal itemTotal = 0.0m;

            if (basket.basketItems != null)
            {
                foreach (BasketItem i in basket.basketItems)
                {
                    bool bMatched = false;
                    if (basket.vouchers != null)
                    {
                        foreach (Voucher v in basket.vouchers)
                        {
                            if (v.products.Contains(i.id))
                            {
                                bMatched = true;
                                //We have voucher for this item, add to the values the discounted value...
                                if (v.voucherValue.valueType.ToLower() == "percentage")
                                {
                                    basketTotal = basketTotal + (i.price - (i.price / 100 * v.voucherValue.amount));
                                    voucherTotal = voucherTotal + (i.price / 100 * v.voucherValue.amount);
                                    itemTotal = itemTotal + i.price;
                                    break;
                                }
                                else
                                {
                                    basketTotal = basketTotal + (i.price - (i.quantity * v.voucherValue.amount));
                                    voucherTotal = voucherTotal + (i.quantity * v.voucherValue.amount);
                                    itemTotal = itemTotal + i.price;
                                    break;
                                }
                            }
                        }
                    }
                    if (bMatched == false)
                    {
                        //No Voucher to Assign... Just Add to the Basket...
                        basketTotal = basketTotal + i.price;
                        itemTotal = itemTotal + i.price;
                    }
                }
            }
            basket.totalBasket = basketTotal;
            basket.totalVouchers = voucherTotal;
            basket.totalItems = itemTotal;
            await _repository.UpdateObject<Basket>(basket, "Basket");
            return basket;
        }

        public async Task<Basket> AddVoucherToBasket(string basketId, Voucher voucher)
        {
            Basket basket = await GetBasketAsyncByID(basketId);
            if (basket == null)
                return null;

            if (basket.vouchers == null)
                basket.vouchers = new List<Voucher>();

            basket.vouchers.Add(voucher);
            await UpdateBasketAsync(basketId, basket);
            return basket;
        }

        public async Task<List<Voucher>> GetVouchersForBasket(string basketId)
        {
            Basket basket = await GetBasketAsyncByID(basketId);
            if (basket == null)
                return null;

            if (basket.vouchers == null)
                return new List<Voucher>();
            else
                return basket.vouchers;
        }

        public async Task<Voucher> UpdateVoucher(string basketId, Voucher voucher)
        {
            Basket basket = await GetBasketAsyncByID(basketId);
            if (basket == null)
                return null;

            if (basket.vouchers == null)
            {
                basket.vouchers = new List<Voucher>();
                basket.vouchers.Add(voucher);
            }
            else
            {
                List<Voucher> vouchers = new List<Voucher>();
                foreach (Voucher v in basket.vouchers)
                {
                    if (v.code != voucher.code)
                        vouchers.Add(v);
                    else
                        vouchers.Add(voucher);
                }
                basket.vouchers = vouchers;
                await UpdateBasketAsync(basketId, basket);
            }
            return voucher;
        }
    }
}